import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './section-2/account/account.component';
import { DevAccountComponent } from './section-2/account/dev-account/dev-account.component';

const routes: Routes = [{ path: 'question1', component: DevAccountComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
