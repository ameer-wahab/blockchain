import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'blockchain-training';

  private digits: number = 100;
  private multipleBig: string;
  public sumValue: number;
  private output = []
  constructor() { }


  ngOnInit() {
    let multiplyVal = (String(this.factorialize(BigInt(this.digits))));
    this.factorializeSum(multiplyVal);
  }

  factorialize = (num) => {
    if (num === 0n) return 1n;
    return num * this.factorialize(num - 1n);
  };


  factorializeSum(multipleValue: any) {
    for (var i = 0, len = multipleValue.length; i < len; i += 1) {
      this.output.push(+multipleValue.charAt(i));
    }

    for (var i = 0, sum = 0; i < this.output.length; sum += this.output[i++]);

    this.sumValue = sum;
  }
}
