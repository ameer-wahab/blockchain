import { Component, OnInit } from '@angular/core';
import { AccountComponent } from '../account.component';

@Component({
  selector: 'app-dev-account',
  templateUrl: './dev-account.component.html',
  styleUrls: ['./dev-account.component.scss']
})
export class DevAccountComponent extends AccountComponent implements OnInit {
  accountDetails: any;
  amount: number;
  accountName: string;
  transferAmount: number
  constructor() {
    super();

  }

  ngOnInit(): void {
    this.detailAccount = [{ id: 1, name: 'ali', balance: '123' }];
  }

  userDepo(info: any, balance: number) {

    let userAccount = { id: 2, name: info }
    let details = this.deposit(userAccount, balance);
    console.info("details ", details);
    return this.curentBalance(details);
  }

  curentBalance(accountDetails?: any) {
    if (accountDetails !== null && accountDetails !== undefined) {
      return this.accountDetails = this.message(accountDetails);
    } else {
      return this.accountDetails = this.message(this.detailAccount[0]);
    }
  }

  setBalance(amount: number) {
    var foundIndex = this.detailAccount.findIndex(x => x.id == 1);
    let balance = { balance: amount };
    const returnedTarget = Object.assign(this.detailAccount[foundIndex], balance);

    console.info("balance", returnedTarget);

  }

}
