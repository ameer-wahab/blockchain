import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevAccountComponent } from './dev-account.component';

describe('DevAccountComponent', () => {
  let component: DevAccountComponent;
  let fixture: ComponentFixture<DevAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DevAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DevAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
