import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  detailAccount: any = [];
  amountBalance: number;
  constructor() {
  }

  ngOnInit(): void {
  }

  deposit(info: any, amount: number) {
    this.detailAccount.push({ id: info.id, name: info.name, balance: amount });
    return { id: info.id, name: info.name, balance: amount };
  }

  withdraw(info: any, amount: number) {
    this.detailAccount = [{
      'id': info.id,
      'name': info.name,
      'balance': amount
    }];

    return this.message(this.detailAccount);
  }

  message(info) {
    console.info("info details ", info);
    return `Account Name: ${info.name}'s, 
    Account Balance: ${info.balance}`;
  }
}
